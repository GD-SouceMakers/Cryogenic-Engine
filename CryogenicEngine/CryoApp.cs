﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Numerics;

using SDL2;

using VulkanCore.Ext;
using VulkanCore.Khr;
using VulkanCore;

using CryogenicEngine.Host;
using CryogenicEngine.CryoVulkan;
using CryogenicEngine.CryoInput;
using CryogenicEngine.CryoRender;

using static CryogenicEngine.CryoMath.TransformHelpers;
using CryogenicEngine.CryoWorld;

namespace CryogenicEngine
{
    public class CryoApp : IDisposable
    {
        public static CryoApp Instance;

        public SDLContext Host { get; private set; }

        public CryoVulkan.CryoVulkan cryoVulkan;
        //private CryoRender.BaseRenderer baseRenderer;
        CryoRenderer cryoRenderer;
        CryoInput.CryoInput cryoInput;

        public CryoContent.CryoContentLoader contentLoader;

        internal CryoWorld.CryoMap map;

        Timer timer;

        internal CryoMaterial defaultMat;

        public CryoApp()
        {
            Instance = this;
        }

        public void Initialize()
        {
            InitSystems();

            CryoRender.CryoShader shader = new CryoShader();
            shader.Initialize(this, cryoRenderer);
            defaultMat = new CryoMaterial(shader);
            defaultMat.CreateTextureSampler();

            map = contentLoader.Load<CryoWorld.CryoMap>("Maps/test1.vmf");
            map.AddComponent(new TestCube(null, new CryoMath.Transform(new CryoMath.Vector3(0, 0, 60), new CryoMath.Vector3(0, 0, 0), new CryoMath.Vector3(1, 1, 1))));
            //map.AddComponent(new TestCube(null, new CryoMath.Transform(new Vector3(0, 0, 60), new Vector3(0, 0, 0), new Vector3(1, 1, 1))));

            //contentLoader.Load<CryoVulkan.CryoVulkanImage>("Textures/IndustryForgedDark512.ktx");

            map.PreSetup();
            map.Setup();

            //Mesh mesh = new Mesh(vertices, indices);
            //cryoRenderer.AddMeshToRender(mesh, defaultMat, new CryoMath.Transform(Vector3.Zero,Vector3.Zero,Vector3.One));

            timer = new Timer();
        }

        private void InitSystems()
        {
            //Init the SDL context
            Host = new SDLContext();
            Host.Initialize();
            Host.CreateWindow();
            Host.window.Initialize();
            Host.PostWindowCreate();

            cryoInput = new CryoInput.CryoInput(this);
            cryoInput.AddBinding(new InputBinding("Forward", true,false, new List<IBindingEvent> {
                new BindingEventKeyboard(SDL.SDL_Keycode.SDLK_UP),
                new BindingEventKeyboard(SDL.SDL_Keycode.SDLK_w)
            }));
            cryoInput.AddBinding(new InputBinding("Back", true,false, new List<IBindingEvent> {
                new BindingEventKeyboard(SDL.SDL_Keycode.SDLK_DOWN),
                new BindingEventKeyboard(SDL.SDL_Keycode.SDLK_s)
            }));
            cryoInput.AddBinding(new InputBinding("MouseH", true,true, new List<IBindingEvent> {
                new BindingEventMouse(BindingEventMouse.Direction.Horizontal)
            }));
            cryoInput.AddBinding(new InputBinding("MouseV", true, true, new List<IBindingEvent> {
                new BindingEventMouse(BindingEventMouse.Direction.Vertical)
            }));

            //The asset mannager
            contentLoader = new CryoContent.CryoContentLoader(this, "./Content/");

            //The Vulkan Subsystem
            cryoVulkan = new CryoVulkan.CryoVulkan();
            cryoVulkan.Initialize(this);

            //The Rendering subsystem
            cryoRenderer = new CryoRenderer(this);
            cryoRenderer.Initialize();
        }

        public void Start()
        {
            timer.Start();


            bool start = true;
            while (start)
            {
                Host.CheckForSDLErrors();

                while (SDL.SDL_PollEvent(out Host.eventInfo) != 0)
                {
                    if (Host.eventInfo.type == SDL.SDL_EventType.SDL_QUIT)
                    {
                        start = false;
                    }
                    else
                    {
                        cryoInput.RegisterEvent(Host.eventInfo);
                    }
                }

                Tick();

                cryoInput.ClearBindingStates();
            }
        }

        public void Tick()
        {
            timer.Tick();

            Update();

            UpdateRenderData();

            //baseRenderer.Draw(timer);
            //cryoRenderer.DrawFrame();
            cryoRenderer.NewDrawFrame();
        }

        private void UpdateRenderData()
        {
            //Update the camera Buffer object
            cryoRenderer.UpdateNodeDatas();
            //Udate all the render nodes
            cryoRenderer.UpdateCameraDatas();
            //Update the Material stuff

        }

        public void Update()
        {
            map.Update();
        }

        public void Dispose()
        {

        }
    }
}

