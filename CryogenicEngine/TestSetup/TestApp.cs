﻿using SDL2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VulkanCore;
using VulkanCore.Khr;
using VulkanCore.Ext;
using System.Diagnostics;

namespace CryogenicEngine
{

    class TestApp
    {
        public IntPtr WindowHandle => GetWindowHandle().info.win.window;
        public IntPtr WindowHInstance => Process.GetCurrentProcess().Handle;

        public SDL.SDL_SysWMinfo GetWindowHandle()
        {
            SDL.SDL_SysWMinfo wMinfo = new SDL.SDL_SysWMinfo();
            SDL.SDL_GetWindowWMInfo(_window, ref wMinfo);
            return wMinfo;
        }
        private IntPtr _window;


        VulkanContext vulkanContext;
        SurfaceKhr surface;
        SwapchainKhr swapchain;
        Image[] swapchainImages;
        CommandBuffer[] CommandBuffers;
        Semaphore ImageAvailableSemaphore;
        Semaphore RenderingFinishedSemaphore;

        public TestApp()
        {

        }

        public void Init()
        {
            SDL.SDL_Init(SDL.SDL_INIT_EVERYTHING);

            _window = SDL.SDL_CreateWindow("test", 1000, 300, 500, 500, SDL.SDL_WindowFlags.SDL_WINDOW_ALLOW_HIGHDPI);



            var availableLayers = Instance.EnumerateLayerProperties();

            var createInfo = new InstanceCreateInfo();
            createInfo.EnabledLayerNames = new[] { Constant.InstanceLayer.LunarGStandardValidation }
                .Where(availableLayers.Contains)
                .ToArray();

            createInfo.EnabledExtensionNames = new[]
            {
                    Constant.InstanceExtension.KhrSurface,
                    Constant.InstanceExtension.KhrWin32Surface,
                    Constant.InstanceExtension.ExtDebugReport
                };

            Instance VKinstance = new Instance(createInfo);

            surface = VKinstance.CreateWin32SurfaceKhr(new Win32SurfaceCreateInfoKhr(WindowHInstance, WindowHandle));

            vulkanContext = new VulkanContext(VKinstance, surface, Platform.Win32);

            ImageAvailableSemaphore = vulkanContext.Device.CreateSemaphore();
            RenderingFinishedSemaphore = vulkanContext.Device.CreateSemaphore();

            swapchain = CreateSwapchain();
            swapchainImages = swapchain.GetImages();
            CommandBuffers = vulkanContext.GraphicsCommandPool.AllocateBuffers(
                new CommandBufferAllocateInfo(CommandBufferLevel.Primary, swapchainImages.Length));


            RecordCommandBuffers();
        }


        private SwapchainKhr CreateSwapchain()
        {
            SurfaceCapabilitiesKhr capabilities = vulkanContext.PhysicalDevice.GetSurfaceCapabilitiesKhr(surface);
            SurfaceFormatKhr[] formats = vulkanContext.PhysicalDevice.GetSurfaceFormatsKhr(surface);
            PresentModeKhr[] presentModes = vulkanContext.PhysicalDevice.GetSurfacePresentModesKhr(surface);
            Format format = formats.Length == 1 && formats[0].Format == Format.Undefined
                ? Format.B8G8R8A8UNorm
                : formats[0].Format;
            PresentModeKhr presentMode =
                presentModes.Contains(PresentModeKhr.Mailbox) ? PresentModeKhr.Mailbox :
                presentModes.Contains(PresentModeKhr.FifoRelaxed) ? PresentModeKhr.FifoRelaxed :
                presentModes.Contains(PresentModeKhr.Fifo) ? PresentModeKhr.Fifo :
                PresentModeKhr.Immediate;

            return vulkanContext.Device.CreateSwapchainKhr(new SwapchainCreateInfoKhr(
                surface,
                format,
                capabilities.CurrentExtent,
                preTransform: capabilities.CurrentTransform,
                presentMode: presentMode));
        }

        public void Tick()
        {
            Draw();
        }

        protected void Draw()
        {
            // Acquire an index of drawing image for this frame.
            int imageIndex = swapchain.AcquireNextImage(semaphore: ImageAvailableSemaphore);

            // Submit recorded commands to graphics queue for execution.
            vulkanContext.GraphicsQueue.Submit(
                ImageAvailableSemaphore,
                PipelineStages.Transfer,
                CommandBuffers[imageIndex],
                RenderingFinishedSemaphore
            );

            // Present the color output to screen.
            vulkanContext.PresentQueue.PresentKhr(RenderingFinishedSemaphore, swapchain, imageIndex);
        }

        private void RecordCommandBuffers()
        {
            var subresourceRange = new ImageSubresourceRange(ImageAspects.Color, 0, 1, 0, 1);
            for (int i = 0; i < CommandBuffers.Length; i++)
            {
                CommandBuffer cmdBuffer = CommandBuffers[i];
                cmdBuffer.Begin(new CommandBufferBeginInfo(CommandBufferUsages.SimultaneousUse));

                if (vulkanContext.PresentQueue != vulkanContext.GraphicsQueue)
                {
                    var barrierFromPresentToDraw = new ImageMemoryBarrier(
                        swapchainImages[i], subresourceRange,
                        Accesses.MemoryRead, Accesses.ColorAttachmentWrite,
                        ImageLayout.Undefined, ImageLayout.PresentSrcKhr,
                        vulkanContext.PresentQueue.FamilyIndex, vulkanContext.GraphicsQueue.FamilyIndex);

                    cmdBuffer.CmdPipelineBarrier(
                        PipelineStages.ColorAttachmentOutput,
                        PipelineStages.ColorAttachmentOutput,
                        imageMemoryBarriers: new[] { barrierFromPresentToDraw });
                }

                RecordCommandBuffer(cmdBuffer, i);

                if (vulkanContext.PresentQueue != vulkanContext.GraphicsQueue)
                {
                    var barrierFromDrawToPresent = new ImageMemoryBarrier(
                        swapchainImages[i], subresourceRange,
                        Accesses.ColorAttachmentWrite, Accesses.MemoryRead,
                        ImageLayout.PresentSrcKhr, ImageLayout.PresentSrcKhr,
                        vulkanContext.GraphicsQueue.FamilyIndex, vulkanContext.PresentQueue.FamilyIndex);

                    cmdBuffer.CmdPipelineBarrier(
                        PipelineStages.ColorAttachmentOutput,
                        PipelineStages.BottomOfPipe,
                        imageMemoryBarriers: new[] { barrierFromDrawToPresent });
                }

                cmdBuffer.End();
            }
        }


        protected void RecordCommandBuffer(CommandBuffer cmdBuffer, int imageIndex)
        {
            var imageSubresourceRange = new ImageSubresourceRange(ImageAspects.Color, 0, 1, 0, 1);

            var barrierFromPresentToClear = new ImageMemoryBarrier(
                swapchainImages[imageIndex], imageSubresourceRange,
                Accesses.None, Accesses.TransferWrite,
                ImageLayout.Undefined, ImageLayout.TransferDstOptimal);

            var barrierFromClearToPresent = new ImageMemoryBarrier(
                swapchainImages[imageIndex], imageSubresourceRange,
                Accesses.TransferWrite, Accesses.MemoryRead,
                ImageLayout.TransferDstOptimal, ImageLayout.PresentSrcKhr);

            cmdBuffer.CmdPipelineBarrier(
                PipelineStages.Transfer, PipelineStages.Transfer,
                imageMemoryBarriers: new[] { barrierFromPresentToClear });
            cmdBuffer.CmdClearColorImage(
                swapchainImages[imageIndex],
                ImageLayout.TransferDstOptimal,
                new ClearColorValue(new ColorF4(1f, 0.58f, 0f, 1.0f)),
                imageSubresourceRange);
            cmdBuffer.CmdPipelineBarrier(
                PipelineStages.Transfer, PipelineStages.Transfer,
                imageMemoryBarriers: new[] { barrierFromClearToPresent });
        }

        

    }
}
