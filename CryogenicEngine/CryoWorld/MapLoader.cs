﻿using CryogenicEngine.CryoWorld;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Numerics;
using VMFParser;
using CryogenicEngine.CryoRender;
using CryogenicEngine.CryoMath;

namespace CryogenicEngine.CryoContent
{
    internal static partial class Loader
    {
        public static CryoMap MapLoader(string path)
        {
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader reader = new StreamReader(stream);

            List<string> file = new List<string>();
            while (!reader.EndOfStream)
            {
                file.Add(reader.ReadLine());
            }


            VMF vMF = new VMF(file.ToArray());

            CryoMap map = new CryoMap();

            VBlock world = (VBlock)vMF.Body.Where(i => (i.Name == "world")).First();

            var solids = world.Body.Where(i => (i.Name == "solid")).Select(i => (VBlock)i).ToList();

            foreach (var item in solids)
            {
                map.AddComponent(LoadBrush(item));
            }

            var entities = vMF.Body.Where(i => (i.Name == "entity"));

            foreach (VBlock item in entities)
            {
                var name = ((VProperty)item.Body.Where(i => i.Name == "classname").First()).Value;

                switch (name)
                {
                    case "info_player_start":
                        map.AddComponent(LoadCamera(item));
                        break;
                    default:
                        break;
                }
            }



            return map;
        }

        static BrushEntity LoadBrush(VBlock root)
        {
            var sidesData = root.Body.Where(i => (i.Name == "side")).Select(i => (VBlock)i).ToList();

            List<Mesh> sides = new List<Mesh>();

            foreach (var side in sidesData)
            {
                List<Vertex> vertex = new List<Vertex>();

                var vertexDatas = (VBlock)side.Body.Where(i => (i.Name == "vertex")).First();

                Regex regex = new Regex("vertex.");

                Vector2[] uv = new[] {
                    new Vector2(1.0f, 0f),
                    new Vector2(0.0f, 0f),
                    new Vector2(0.0f, 1.0f),
                    new Vector2(1.0f, 1.0f)
                };
                int k = 0;

                foreach (var vertexData in vertexDatas.Body)
                {
                    if (regex.IsMatch(vertexData.Name))
                    {
                        var cords = ((VProperty)vertexData).Value;
                        var cord = cords.Split(' ');

                        CryoMath.Vector3 pos = new CryoMath.Vector3(float.Parse(cord[0]), float.Parse(cord[1]), float.Parse(cord[2]));

                        vertex.Add(new Vertex(pos, new CryoMath.Vector3(0, 0, 0), uv[k]));

                        k++;
                    }
                }

                sides.Add(new Mesh(vertex.ToArray(), new[] { 0, 1, 2, 2, 3, 0 }));
            }

            return new BrushEntity(null, Transform.Identity, sides);
        }

        static CameraEntity LoadCamera(VBlock root)
        {
            //VBlock camera = (VBlock)root.Body.Where(i => i.Name == "camera").First();

            var position = ((VMFParser.VProperty)root.Body.Where(i => i.Name == "origin").First()).Value.Split(' ');

            var look = ((VMFParser.VProperty)root.Body.Where(i => i.Name == "angles").First()).Value.Split(' ');

            var transform = new Transform(
                new CryoMath.Vector3(float.Parse(position[0]), float.Parse(position[1]), float.Parse(position[2])),
                new CryoMath.Vector3(float.Parse(look[2]), float.Parse(look[0]), float.Parse(look[1])),
                new CryoMath.Vector3(1, 1, 1)
                );

            return new CameraEntity(null, transform);
        }
    }
}
