﻿using System;
using System.Collections.Generic;
using System.Text;

using CryogenicEngine.CryoMath;
using CryogenicEngine.CryoContent;
using CryogenicEngine.CryoRender;

namespace CryogenicEngine.CryoWorld
{
    class MeshRenderer : Component
    {
        Mesh mesh;

        public MeshRenderer(Component parent, Mesh mesh) : base(parent)
        {
            this.mesh = mesh;
        }

        internal override void Setup(CryoMap map)
        {
            base.Setup(map);

            Transform t = Transform.Identity;
            if (parentTransform != null)
                t = parentTransform;

            //if (t != Transform.Identity)
            //throw new Exception("asd");

            //CryoRenderer.Instance.AddMeshToRender(mesh,CryoApp.Instance.defaultMat,t);

            CryoRenderer.Instance.AddMeshToRender(mesh, CryoApp.Instance.defaultMat, GetTransform);

        }

        public Transform GetTransform()
        {
            Transform t = Transform.Identity;
            if (parentTransform != null)
                t = parentTransform;

            return t;
        }
    }
}
