﻿using CryogenicEngine.CryoMath;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CryogenicEngine.CryoWorld
{
    public class Component
    {
        internal List<Component> internalHierarchy = new List<Component>();

        List<Component> hierarchy = new List<Component>();

        internal Component parent;
        internal Transform parentTransform;

        public Component(Component parent)
        {
            this.parent = parent;
            if (parent != null)
                this.parentTransform = parent.parentTransform;
            else
                this.parentTransform = Transform.Identity;
        }

        internal virtual void PreSetup(CryoMap cryoMap)
        {
            foreach (var item in hierarchy)
            {
                item.PreSetup(cryoMap);
            }
        }

        internal virtual void Setup(CryoMap map)
        {
            foreach (var item in hierarchy)
            {
                item.Setup(map);
            }
        }
    }
}


/*
 * Behaviour        :       Any calass that is interacting with the engine
 * - Component      : (*)   Can be in hierarchy but has no transform of it's self - used for data/game logic - for transform manip. it uses the parent's transform
 *  - Entity        : (-)   Has transform
 * - WorldComp      : (+)   for world logic
 *  
 *  
 *  
 *  World
 *  + LevelStuff
 *  - Mesh
 *  - Mesh
 *  - Mesh
 *  - Player
 *    * Movement
 *      - Camera
 *      - Mesh
 *  - Button
 *      - Mesh
 *      - Trigger
 *  - Door
 *      - Transform Anim
 *          - Mesh
 *      - Trigger
 *      
*/