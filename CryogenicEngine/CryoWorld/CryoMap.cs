﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CryogenicEngine.CryoWorld
{
    internal class CryoMap : IDisposable
    {
        List<Component> hierarchy = new List<Component>();

        internal CameraEntity activeCamera;

        internal void Setup()
        {
            foreach (var item in hierarchy)
            {
                item.Setup(this);
            }
        }

        internal void PreSetup()
        {
            foreach (var item in hierarchy)
            {
                item.PreSetup(this);
            }
        }

        internal void AddComponent(Component c)
        {
            hierarchy.Add(c);
        }

        internal void SetActiveCamera(CameraEntity  camera)
        {
            activeCamera = camera;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public delegate void UpdateDelegate();
        UpdateDelegate updateCall;

        public void Update()
        {
            updateCall();
        }

        public void SubscribeUpdate(UpdateDelegate update)
        {
            updateCall += update;
        }
    }
}
