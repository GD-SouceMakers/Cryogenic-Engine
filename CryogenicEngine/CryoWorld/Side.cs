﻿using System.Collections.Generic;
using System.Numerics;

using CryogenicEngine.CryoRender;

namespace CryogenicEngine.CryoWorld
{
    class Side
    {
        List<Vertex> vertex = new List<Vertex>();

        public Side(CryoMath.Vector3 vert0, CryoMath.Vector3 vert1, CryoMath.Vector3 vert2, CryoMath.Vector3 vert3)
        {
            vertex.Add(new Vertex(vert0, new CryoMath.Vector3(0, 0, 0), new Vector2(0, 0)));
            vertex.Add(new Vertex(vert1, new CryoMath.Vector3(0, 0, 0), new Vector2(0, 0)));
            vertex.Add(new Vertex(vert2, new CryoMath.Vector3(0, 0, 0), new Vector2(0, 0)));
            vertex.Add(new Vertex(vert3, new CryoMath.Vector3(0, 0, 0), new Vector2(0, 0)));
        }

        public Side(List<Vertex> vectors)
        {
            vertex.AddRange(vectors);
        }
    }
}