﻿using CryogenicEngine.CryoMath;

namespace CryogenicEngine.CryoWorld
{
    public class Entity : Component
    {
        public Transform transform;

        

        public Entity(Component parent, Transform transform):base(parent)
        {
            this.transform = transform;
            this.parentTransform = transform;
        }
    }
}
