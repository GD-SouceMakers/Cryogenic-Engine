﻿using CryogenicEngine.CryoMath;
using CryogenicEngine.CryoRender;
using System;
using System.Collections.Generic;
using System.Text;

namespace CryogenicEngine.CryoWorld
{
    class BrushEntity : Entity
    {
        internal List<Mesh> _sides = new List<Mesh>();

        public BrushEntity(Component c, Transform t,List<Mesh> sides):base(c,t)
        {
            _sides = sides;

            foreach (var item in _sides)
            {
                internalHierarchy.Add(new MeshRenderer(this,item));
            }
        }

        internal override void Setup(CryoMap map)
        {
            base.Setup(map);
            foreach (var item in internalHierarchy)
            {
                item.Setup(map);
            }
        }
    }
}
