﻿using CryogenicEngine.CryoMath;
using CryogenicEngine.CryoRender;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace CryogenicEngine.CryoWorld
{
    public class TestCube : Entity
    {
        float angle = 0;

        public static string Name = "test_cube";

        public TestCube(Component parent, Transform transform) : base(parent, transform)
        {
            internalHierarchy.Add(new MeshRenderer(this, Mesh.Box(64,64,64)));

            
        }

        internal override void Setup(CryoMap map)
        {
            map.SubscribeUpdate(Update);

            foreach (var item in internalHierarchy)
            {
                item.Setup(map);
            }

            base.Setup(map);
        }

        void Update()
        {
            angle += 0.1f;
            transform.SetRotation(new CryoMath.Vector3(angle,angle,angle));

            //Console.WriteLine("X: {0}, Y: {1}, Z: {2}", transform.rotation.X, transform.rotation.Y, transform.rotation.Z);
            //Console.WriteLine(transform.rotation.Z);
        }
    }
}
