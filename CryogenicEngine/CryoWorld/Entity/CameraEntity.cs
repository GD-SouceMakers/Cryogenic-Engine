﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

using CryogenicEngine.CryoContent;
using CryogenicEngine.CryoMath;
using CryogenicEngine.CryoRender;
using static CryogenicEngine.CryoMath.TransformHelpers;


namespace CryogenicEngine.CryoWorld
{
    class CameraEntity : Entity
    {
        float angleZ = 0;
        float angleY = 0;

        public CameraEntity(Component parent, Transform transform) : base(parent, transform)
        {
        }

        internal override void PreSetup(CryoMap map)
        {
            base.PreSetup(map);

            map.SetActiveCamera(this);
            map.SubscribeUpdate(Update);

            var a = new CryoRenderer.CameraOptions
            {
                RenderTarget = 0
            };

            CryoRender.CryoRenderer.Instance.AddCamera(UpdateCameraRenderData);
        }

        void Update()
        {
            var a = CryoInput.CryoInput.Instance.GetInputState("Forward");
            transform.position.X += a;
            a = CryoInput.CryoInput.Instance.GetInputState("Back");
            transform.position.X -= a;

            float z = CryoInput.CryoInput.Instance.GetInputState("MouseH") / 10f;
            float y = CryoInput.CryoInput.Instance.GetInputState("MouseV") / 10f;
            angleZ += z * -1;
            angleY += y;

            transform.SetRotation(new CryoMath.Vector3(0, angleY, angleZ));

        }

        internal CameraBufferObject UpdateCameraRenderData()
        {
            return new CameraBufferObject
            {
                view = GetViewMatrix(),
                proj = GetProjectonMatrix()
            };
        }

        internal CryoMath.Matrix4x4 GetViewMatrix()
        {
            //TODO: Fix this mess, make a proper view matrix and not just a Look AT

            //Quaternion quaternion = Quaternion.CreateFromYawPitchRoll(transform.rotation.Z, transform.rotation.Y, transform.rotation.X);

            CryoMath.Vector3 lookAt = CryoMath.Vector3.UnitX;
            CryoMath.Vector3 a = CryoMath.Vector3.Transform(lookAt, transform.rotation);
            CryoMath.Vector3 b = a + transform.position;


            CryoMath.Matrix4x4 matrix = CryoMath.Matrix4x4.CreateLookAt(transform.position, b, transform.GetLocalUp());

            /*
            Matrix4x4 RotationMatrix = Matrix4x4.CreateRotationX(Radian(transform.rotation.X));
            RotationMatrix *= Matrix4x4.CreateRotationZ(Radian(transform.rotation.Z));
            RotationMatrix *= Matrix4x4.CreateRotationY(Radian(transform.rotation.Y));
            //Matrix4x4 RotationMatrix = Matrix4x4.CreateFromQuaternion(quaternion);
            Matrix4x4 TranslationMatrix = Matrix4x4.CreateTranslation(transform.position);
            //Matrix4x4 ScaleMatrix = Matrix4x4.CreateScale(transform.scale);
            
            Matrix4x4 res = Matrix4x4.Identity;
            Matrix4x4.Invert(transform.GetWorldMatrix(), out res);
            */

            return matrix;
        }

        internal CryoMath.Matrix4x4 GetProjectonMatrix()
        {
            var proj = CryoMath.Matrix4x4.CreatePerspectiveFieldOfView(Radian(60.0f), CryoApp.Instance.Host.window.Width / (CryoApp.Instance.Host.window.Height * 1.0f), 0.1f, 1000.0f);
            proj.M22 *= -1;

            return proj;
        }
    }
}
