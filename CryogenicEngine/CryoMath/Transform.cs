﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CryogenicEngine.CryoMath
{
    public class Transform
    {
        public CryoMath.Vector3 position;
        public CryoMath.Quaternion rotation;
        public CryoMath.Vector3 scale;

        public Transform(Vector3 position, Vector3 rotation, Vector3 scale)
        {
            this.position = position;
            //this.rotation = Quaternion.CreateFromYawPitchRoll(rotation.Z, rotation.Y, rotation.X);
            this.scale = scale;

            SetRotation(rotation);
        }

        public void SetRotation(Vector3 euler)
        {
            Quaternion quaternion = new Quaternion(0, 0, 0, 1);
            quaternion *= Quaternion.CreateFromAxisAngle(Vector3.UnitX, TransformHelpers.Radian(euler.X));
            quaternion *= Quaternion.CreateFromAxisAngle(Vector3.UnitY, TransformHelpers.Radian(euler.Y));
            quaternion *= Quaternion.CreateFromAxisAngle(Vector3.UnitZ, TransformHelpers.Radian(euler.Z));

            //rotation = Quaternion.CreateFromYawPitchRoll(TransformHelpers.Radian(euler.X), TransformHelpers.Radian(euler.Z), TransformHelpers.Radian(euler.Y));
            rotation = quaternion;

        }
        /*
        public Vector3 GetRotation()
        {
            var q = rotation;

            MathF.Atan2(2 * (q.X * q.Y + q.W * q.Z), q.W * q.W + q.X * q.X - q.Y * q.Y - q.Z * q.Z);

        }
        */
        public CryoMath.Matrix4x4 GetWorldMatrix()
        {
            //Console.WriteLine("X: {0}, Y: {1}, Z: {2}", rotation.X, rotation.Y, rotation.Z);
            Matrix4x4 RotationMatrix = Matrix4x4.CreateFromQuaternion(rotation);
            Matrix4x4 TranslationMatrix = Matrix4x4.CreateTranslation(position);
            Matrix4x4 ScaleMatrix = Matrix4x4.CreateScale(scale);

            return RotationMatrix * TranslationMatrix * ScaleMatrix;
        }

        public Vector3 GetLocalUp()
        {
            /*glm::vec3 camFront;
				camFront.x = -cos(glm::radians(rotation.x)) * sin(glm::radians(rotation.y));
				camFront.y = sin(glm::radians(rotation.x));
				camFront.z = cos(glm::radians(rotation.x)) * cos(glm::radians(rotation.y));
				camFront = glm::normalize(camFront);
                */
            Vector3 up = Vector3.UnitZ;

            return up.Rotate(rotation);
        }


        public static Transform Identity = new Transform(Vector3.Zero, Vector3.Zero, Vector3.One);
    }
}
