﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CryogenicEngine.CryoMath
{
    public class CryoMathConsatants
    {
        /// <summary>
        /// A small value often used to decide if numeric 
        /// results are zero.
        /// </summary>
        public const float Epsilon = 1.192092896e-012f;
    }
}
