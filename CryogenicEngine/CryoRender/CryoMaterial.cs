﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using VulkanCore;

namespace CryogenicEngine.CryoRender
{
    class CryoMaterial
    {
        CryoRenderer cryoRenderer;

        internal CryoShader Shader;

        internal DescriptorSet descriptorSet;

        Sampler sampler;
        CryoVulkan.CryoVulkanImage testTexture;

        public CryoMaterial(CryoShader shader)
        {
            cryoRenderer = CryoRenderer.Instance;

            testTexture = CryoApp.Instance.contentLoader.Load<CryoVulkan.CryoVulkanImage>("Textures/IndustryForgedDark512.ktx");

            Shader = shader;
        }

        internal WriteDescriptorSet[] GetWriteDescriptorSets(DescriptorSet descriptorSet)
        {
            List<WriteDescriptorSet> writeDescriptorSets = new List<WriteDescriptorSet>
            {
                new WriteDescriptorSet(descriptorSet, 2, 0, 1, DescriptorType.CombinedImageSampler,
                    imageInfo: new[] { new DescriptorImageInfo(sampler, testTexture.View, ImageLayout.General) })
            };

            return writeDescriptorSets.ToArray();
        }

        public void CreateTextureSampler()
        {
            var createInfo = new SamplerCreateInfo
            {
                MagFilter = Filter.Linear,
                MinFilter = Filter.Linear,
                MipmapMode = SamplerMipmapMode.Linear
            };
            // We also enable anisotropic filtering. Because that feature is optional, it must be
            // checked if it is supported by the device.
            if (CryoVulkan.CryoVulkan.Instance.Features.SamplerAnisotropy)
            {
                createInfo.AnisotropyEnable = true;
                createInfo.MaxAnisotropy = CryoVulkan.CryoVulkan.Instance.Properties.Limits.MaxSamplerAnisotropy;
            }
            else
            {
                createInfo.MaxAnisotropy = 1.0f;
            }
            sampler = CryoVulkan.CryoVulkan.Instance.Device.CreateSampler(createInfo);
        }
    }
}
