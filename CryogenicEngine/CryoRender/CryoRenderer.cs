﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using VulkanCore;
using VulkanCore.Khr;

using System.Numerics;

using CryogenicEngine.CryoVulkan;
//using CryogenicEngine.CryoMath;

using static CryogenicEngine.CryoMath.TransformHelpers;

using System.Runtime.InteropServices;

namespace CryogenicEngine.CryoRender
{
    class CryoRenderer : IDisposable
    {
        public static CryoRenderer Instance;

        CryoVulkan.CryoVulkan cryoVulkan;
        CryogenicEngine.CryoApp cryoApp;

        internal delegate void Draw(CommandBuffer commandbuffer);
        internal Draw drawCalls;

        public delegate CryoMath.Transform Update();

        struct RenderNode
        {
            public Update update;
            public Mesh mesh;
            public CryoMaterial mat;
            public DescriptorSet descriptorSet;
            public CryoMannagedBuffer uniformBuffer;
        }

        List<RenderNode> renderNodes = new List<RenderNode>();

        //Swapchain
        private SwapchainKhr swapchain;
        private Image[] swapchainImages;
        private ImageView[] imageViews;

        //Depth Buffer
        private CryoVulkanImage depthImage;

        //Frame Buffer (swapchain and depth)
        private Framebuffer[] framebuffers;

        //Render Pass
        internal RenderPass renderPass;

        //Command Buffers
        private CommandBuffer[] commandBuffers;

        //Semaphores
        Semaphore ImageAvailableSemaphore;
        Semaphore RenderingFinishedSemaphore;

        //TODO do we need this here or per Material
        internal DescriptorPool descriptorPool;
        internal Sampler sampler;

        public static DescriptorSetLayoutBinding[] baseDescriptors = new[] {
                new DescriptorSetLayoutBinding(0, DescriptorType.UniformBuffer, 1, ShaderStages.All),
                new DescriptorSetLayoutBinding(1, DescriptorType.UniformBuffer, 1, ShaderStages.All)
        };

        public CryoRenderer(CryoApp app)
        {
            Instance = this;
            cryoVulkan = CryoVulkan.CryoVulkan.Instance;
            cryoApp = app;
        }

        public void Dispose()
        {
            foreach (var item in swapchainImages)
            {
                item.Dispose();
            }

            renderPass.Dispose();

            foreach (var item in imageViews)
            {
                item.Dispose();
            }

            foreach (var item in framebuffers)
            {
                item.Dispose();
            }
        }

        public void Initialize()
        {
            ImageAvailableSemaphore = cryoVulkan.Device.CreateSemaphore();
            RenderingFinishedSemaphore = cryoVulkan.Device.CreateSemaphore();

            CreateTextureSampler();

            CreateSwapchain();
            CreateImageViews();
            CreateDepthResources();

            CreateRenderPass();
            CreateFramebuffers();

            CreateCommandBuffers();

            CreateDescriptorPool();
        }

        private void CreateTextureSampler()
        {

            var createInfo = new SamplerCreateInfo
            {
                MagFilter = Filter.Linear,
                MinFilter = Filter.Linear,
                MipmapMode = SamplerMipmapMode.Linear
            };
            // We also enable anisotropic filtering. Because that feature is optional, it must be
            // checked if it is supported by the device.
            if (cryoVulkan.Features.SamplerAnisotropy)
            {
                createInfo.AnisotropyEnable = true;
                createInfo.MaxAnisotropy = cryoVulkan.Properties.Limits.MaxSamplerAnisotropy;
            }
            else
            {
                createInfo.MaxAnisotropy = 1.0f;
            }
            sampler = cryoVulkan.Device.CreateSampler(createInfo);
        }

        private void CreateSwapchain()
        {
            SurfaceCapabilitiesKhr capabilities = cryoVulkan.PhysicalDevice.GetSurfaceCapabilitiesKhr(cryoVulkan.surface);

            SurfaceFormatKhr[] formats = cryoVulkan.PhysicalDevice.GetSurfaceFormatsKhr(cryoVulkan.surface);

            PresentModeKhr[] presentModes = cryoVulkan.PhysicalDevice.GetSurfacePresentModesKhr(cryoVulkan.surface);

            Format format = formats.Length == 1 && formats[0].Format == Format.Undefined
                ? Format.B8G8R8A8UNorm
                : formats[0].Format;

            PresentModeKhr presentMode =
                presentModes.Contains(PresentModeKhr.Mailbox) ? PresentModeKhr.Mailbox :
                presentModes.Contains(PresentModeKhr.FifoRelaxed) ? PresentModeKhr.FifoRelaxed :
                presentModes.Contains(PresentModeKhr.Fifo) ? PresentModeKhr.Fifo :
                PresentModeKhr.Immediate;

            swapchain = cryoVulkan.Device.CreateSwapchainKhr(new SwapchainCreateInfoKhr(
                cryoVulkan.surface,
                format,
                capabilities.CurrentExtent,
                
                preTransform: capabilities.CurrentTransform,
                
                presentMode: presentMode));

            swapchainImages = swapchain.GetImages();
        }

        private void CreateImageViews()
        {
            var imageViews = new ImageView[swapchainImages.Length];
            for (int i = 0; i < swapchainImages.Length; i++)
            {
                imageViews[i] = swapchainImages[i].CreateView(new ImageViewCreateInfo(
                    swapchain.Format,
                    new ImageSubresourceRange(ImageAspects.Color, 0, 1, 0, 1)));
            }
            this.imageViews = imageViews;
        }

        private void CreateDepthResources()
        {
            depthImage = CryoVulkan.CryoVulkanImage.DepthStencil(cryoApp.Host.window.Width, cryoApp.Host.window.Height);
        }

        private void CreateFramebuffers()
        {
            var _framebuffers = new Framebuffer[swapchainImages.Length];
            for (int i = 0; i < swapchainImages.Length; i++)
            {
                _framebuffers[i] = renderPass.CreateFramebuffer(new FramebufferCreateInfo(
                    new[] { imageViews[i], depthImage.View },
                    cryoApp.Host.window.Width,
                    cryoApp.Host.window.Height));
            }
            framebuffers = _framebuffers;
        }

        private void CreateRenderPass()
        {
            var subpasses = new[]
            {
                new SubpassDescription(new[] {
                    new AttachmentReference(0, ImageLayout.ColorAttachmentOptimal)
                },
                new AttachmentReference(1, ImageLayout.DepthStencilAttachmentOptimal))
            };
            var attachments = new[]
            {
                new AttachmentDescription
                {
                    Samples = SampleCounts.Count1,
                    Format = swapchain.Format,
                    InitialLayout = ImageLayout.Undefined,
                    FinalLayout = ImageLayout.PresentSrcKhr,
                    LoadOp = AttachmentLoadOp.Clear,
                    StoreOp = AttachmentStoreOp.Store,
                    StencilLoadOp = AttachmentLoadOp.DontCare,
                    StencilStoreOp = AttachmentStoreOp.DontCare
                },

                // Depth attachment.
                new AttachmentDescription
                {
                    Format = depthImage.Format,
                    Samples = SampleCounts.Count1,
                    LoadOp = AttachmentLoadOp.Clear,
                    StoreOp = AttachmentStoreOp.DontCare,
                    StencilLoadOp = AttachmentLoadOp.DontCare,
                    StencilStoreOp = AttachmentStoreOp.DontCare,
                    InitialLayout = ImageLayout.Undefined,
                    FinalLayout = ImageLayout.DepthStencilAttachmentOptimal
                }
            };
            SubpassDependency dependency = new SubpassDependency
            {
                SrcSubpass = Constant.SubpassExternal,
                DstSubpass = 0,
                SrcStageMask = PipelineStages.ColorAttachmentOutput,
                SrcAccessMask = 0,
                DstStageMask = PipelineStages.ColorAttachmentOutput,
                DstAccessMask = Accesses.ColorAttachmentRead | Accesses.ColorAttachmentWrite,
            };

            var createInfo = new RenderPassCreateInfo(subpasses,
                attachments,
                dependencies: new[] { dependency });
            renderPass = cryoVulkan.Device.CreateRenderPass(createInfo);
        }

        private void CreateCommandBuffers()
        {
            commandBuffers = cryoVulkan.GraphicsCommandPool.AllocateBuffers(
                new CommandBufferAllocateInfo(CommandBufferLevel.Primary, swapchainImages.Length));

            for (int i = 0; i < commandBuffers.Length; i++)
            {
                renderQueue.Enqueue(new CoomadBufferQueueElement
                {
                    cmdBuffer = commandBuffers[i],
                    imageAvailableSemaphore = cryoVulkan.Device.CreateSemaphore(),
                    pressentSemaphore = cryoVulkan.Device.CreateSemaphore(),
                    waitFence = cryoVulkan.Device.CreateFence(new FenceCreateInfo { Flags = FenceCreateFlags.Signaled })
                });
            }

        }

        private void CreateDescriptorPool()
        {
            DescriptorPoolSize[] poolSizes = new[] {
                new DescriptorPoolSize(DescriptorType.UniformBuffer, 100),
                new DescriptorPoolSize(DescriptorType.CombinedImageSampler, 100)
            };

            DescriptorPoolCreateInfo poolCreateInfo = new DescriptorPoolCreateInfo(50, poolSizes);

            //TODO error check
            //TODO desposing
            descriptorPool = cryoVulkan.Device.CreateDescriptorPool(poolCreateInfo);
        }


        struct CoomadBufferQueueElement
        {
            public CommandBuffer cmdBuffer;
            public Fence waitFence;
            public Semaphore imageAvailableSemaphore;
            public Semaphore pressentSemaphore;
        }

        Queue<CoomadBufferQueueElement> renderQueue = new Queue<CoomadBufferQueueElement>();

        public void NewDrawFrame()
        {
            //Get the latest one and wait for it to finish
            var currentone = renderQueue.Dequeue();
            currentone.waitFence.Wait();

            currentone.waitFence.Reset();

            //Get a image to use with it
            int imageIndex = swapchain.AcquireNextImage(semaphore: currentone.imageAvailableSemaphore);

            //Record the buffer
            RecordCommandBuffer(imageIndex, currentone.cmdBuffer);

            // Submit recorded commands to graphics queue for execution.
            cryoVulkan.GraphicsQueue.Submit(
                currentone.imageAvailableSemaphore,
                PipelineStages.ColorAttachmentOutput,
                currentone.cmdBuffer,
                currentone.pressentSemaphore,
                currentone.waitFence
            );

            // Present the color output to screen.
            cryoVulkan.PresentQueue.PresentKhr(currentone.pressentSemaphore, swapchain, imageIndex);

            //Console.WriteLine(imageIndex);

            //currentone.waitFence.Wait();

            renderQueue.Enqueue(currentone);
        }

        /*
        public void DrawFrame()
        {
            // Acquire an index of drawing image for this frame.
            int imageIndex = swapchain.AcquireNextImage(semaphore: ImageAvailableSemaphore);

            cryoVulkan.Device.WaitIdle();

            RecordCommandBuffer(imageIndex);

            // Submit recorded commands to graphics queue for execution.
            cryoVulkan.GraphicsQueue.Submit(
                ImageAvailableSemaphore,
                PipelineStages.Transfer,
                commandBuffers[imageIndex],
                RenderingFinishedSemaphore
            );

            // Present the color output to screen.
            cryoVulkan.PresentQueue.PresentKhr(RenderingFinishedSemaphore, swapchain, imageIndex);
        }

        void RecordCommandBuffer(int swapchainIndex)
        {
            CommandBuffer cmdBuffer = commandBuffers[swapchainIndex];
            cmdBuffer.Begin(new CommandBufferBeginInfo(CommandBufferUsages.OneTimeSubmit));

            var renderPassBeginInfo = new RenderPassBeginInfo(
                framebuffers[swapchainIndex],
                new Rect2D(Offset2D.Zero, new Extent2D(cryoApp.Host.window.Width, cryoApp.Host.window.Height)),

                new ClearColorValue(new ColorF4(0.39f, 0.58f, 0.93f, 1.0f)),
                new ClearDepthStencilValue(1.0f, 0)
            );

            cmdBuffer.CmdBeginRenderPass(renderPassBeginInfo);

            drawCalls(cmdBuffer);

            cmdBuffer.CmdEndRenderPass();

            cmdBuffer.End();

        }
    */

        void RecordCommandBuffer(int swapchainIndex, CommandBuffer cmdBuffer)
        {
            cmdBuffer.Begin(new CommandBufferBeginInfo(CommandBufferUsages.OneTimeSubmit));

            var renderPassBeginInfo = new RenderPassBeginInfo(
                framebuffers[swapchainIndex],
                new Rect2D(Offset2D.Zero, new Extent2D(cryoApp.Host.window.Width, cryoApp.Host.window.Height)),

                new ClearColorValue(new ColorF4(0.39f, 0.58f, 0.93f, 1.0f)),
                new ClearDepthStencilValue(1.0f, 0)
            );

            cmdBuffer.CmdBeginRenderPass(renderPassBeginInfo);

            //drawCalls(cmdBuffer);

            foreach (var item in renderNodes)
            {
                cmdBuffer.CmdBindPipeline(PipelineBindPoint.Graphics, item.mat.Shader.pipeline);

                cmdBuffer.CmdBindVertexBuffer(item.mesh.vertexBuffer.Page.DeviceBuffer, item.mesh.vertexBuffer.Offset);
                cmdBuffer.CmdBindIndexBuffer(item.mesh.indexBuffer.Page.DeviceBuffer, item.mesh.indexBuffer.Offset);
                cmdBuffer.CmdBindDescriptorSet(PipelineBindPoint.Graphics, item.mat.Shader.pipelineLayout, item.descriptorSet);

                cmdBuffer.CmdDrawIndexed(item.mesh.indeces.Length);
            }


            cmdBuffer.CmdEndRenderPass();

            cmdBuffer.End();

        }



        //TODO: IMPORTANT: rework
        // This should only ask for MESH and MATERIAL and for a Upade function to get the newer transforms
        // Should get the MATERIAL BUFFER and the CAMERA BUFFER
        
        public void AddMeshToRender(Mesh mesh, CryoMaterial material, Update update)
        {
            if (!mesh.copyed)
            {
                mesh.CopyToMemory();
            }

            var currentNode = new RenderNode
            {
                mat = material,
                mesh = mesh,
                update = update,
                uniformBuffer = CryoVulkanBufferManagger.Instance.CreateBuffer(BufferUsages.UniformBuffer, Marshal.SizeOf<TransformBufferObject>())
        };

            DescriptorSetAllocateInfo allocateInfo = new DescriptorSetAllocateInfo(1, new[] { material.Shader.descriptorSetLayout });

            //TODO error check
            currentNode.descriptorSet = descriptorPool.AllocateSets(allocateInfo)[0];

            List<WriteDescriptorSet> writeDescriptorSets = new List<WriteDescriptorSet>
            {
                new WriteDescriptorSet(currentNode.descriptorSet, 0, 0, 1, DescriptorType.UniformBuffer,
                    bufferInfo: new[] { new DescriptorBufferInfo(renderCameras[0].uniformBuffer,renderCameras[0].uniformBuffer.Offset, renderCameras[0].uniformBuffer.Size) }),
                new WriteDescriptorSet(currentNode.descriptorSet, 1, 0, 1, DescriptorType.UniformBuffer,
                    bufferInfo: new[] { new DescriptorBufferInfo(currentNode.uniformBuffer,currentNode.uniformBuffer.Offset,currentNode.uniformBuffer.Size) })
            };
            writeDescriptorSets.AddRange(currentNode.mat.GetWriteDescriptorSets(currentNode.descriptorSet));

            //WriteDescriptorSet[] descriptorWriters = new[] {
            //    new WriteDescriptorSet(currentNode.descriptorSet, 0, 0, 1, DescriptorType.UniformBuffer,
            //        bufferInfo: new[] { new DescriptorBufferInfo(uniformBuffer) }),
            //    new WriteDescriptorSet(currentNode.descriptorSet, 1, 0, 1, DescriptorType.CombinedImageSampler,
            //        imageInfo: new[] { new DescriptorImageInfo(sampler, currentNode.mat.testTexture.View, ImageLayout.General) })
            //};

            descriptorPool.UpdateSets(writeDescriptorSets.ToArray());

            renderNodes.Add(currentNode);
        }

        //Old way should not be used
        //[System.Obsolete]
        //public void AddMeshToRender(Mesh mesh, CryoMaterial material, Transform transform)
        //{
        //    if (!mesh.copyed)
        //    {
        //        mesh.CopyToMemory();
        //    }

        //    drawCalls += (CommandBuffer cmd) =>
        //    {
        //        //TODO: Check if the transform is dirty

        //        UniformBufferObject ubo = new UniformBufferObject
        //        {
        //            model = transform.GetWorldMatrix(),

        //            //ubo.view = Matrix4x4.CreateLookAt(new Vector3(10f, 2.0f, 10f), new Vector3(0.0f, 0.0f, 0.0f), new Vector3(0.0f, 0.0f, 1.0f));
        //            view = cryoApp.map.activeCamera.GetViewMatrix(),



        //            //TODO: Use the size of the swapchain
        //            /*
        //            ubo.proj = Matrix4x4.CreatePerspectiveFieldOfView(Radian(45.0f), cryoApp.Host.window.Width / cryoApp.Host.window.Height, 0.1f, 1000.0f);
        //            ubo.proj.M22 *= -1;
        //            */

        //            proj = cryoApp.map.activeCamera.GetProjectonMatrix()
        //        };


        //        cmd.CmdBindPipeline(PipelineBindPoint.Graphics, material.Shader.pipeline);

        //        cmd.CmdBindVertexBuffer(mesh.vertexBuffer.Page.DeviceBuffer, mesh.vertexBuffer.Offset);
        //        cmd.CmdBindIndexBuffer(mesh.indexBuffer.Page.DeviceBuffer, mesh.indexBuffer.Offset);
        //        cmd.CmdBindDescriptorSet(PipelineBindPoint.Graphics, material.Shader.pipelineLayout, material.GetDescriptorSet(ubo));

        //        cmd.CmdDrawIndexed(mesh.indeces.Length);
        //    };
        //}

        internal void UpdateNodeDatas()
        {
            foreach (var item in renderNodes)
            {
                var newTransform = item.update().GetWorldMatrix();

                item.uniformBuffer.UpdateData(newTransform);
            }
        }

        #region Camera

        //The representation of the camera to the renderer
        //This would contain 
        public struct RenderCamera
        {
            public UpdateCameraTransforms update;
            public CryoMannagedBuffer uniformBuffer;
        }

        List<RenderCamera> renderCameras = new List<RenderCamera>();

        //This is for the options that are not changing
        //UNUSED: To be actuall y implementd when multi camera setups are supported
        public struct CameraOptions
        {
            public int RenderTarget;
        }

        //This is the return struct of the user given update delegate
        //And the UBO submitted to the shaders

        public delegate CameraBufferObject UpdateCameraTransforms();

        public void AddCamera(UpdateCameraTransforms updateCamera)
        {
            var buffer = CryoVulkanBufferManagger.Instance.CreateBuffer(BufferUsages.UniformBuffer, Marshal.SizeOf<CameraBufferObject>());

            var newCamera = new RenderCamera
            {
                uniformBuffer = buffer,
                update = updateCamera
            };

            renderCameras.Add(newCamera);
        }

        internal void UpdateCameraDatas()
        {
            foreach (var item in renderCameras)
            {
                var newTransforms = item.update();

                item.uniformBuffer.UpdateData(newTransforms);
            }
        }

        #endregion
    }

}

[StructLayout(LayoutKind.Sequential)]
public struct CameraBufferObject
{
    public CryogenicEngine.CryoMath.Matrix4x4 view;
    public CryogenicEngine.CryoMath.Matrix4x4 proj;
};

[StructLayout(LayoutKind.Sequential)]
struct TransformBufferObject
{
    public Matrix4x4 model;
};
