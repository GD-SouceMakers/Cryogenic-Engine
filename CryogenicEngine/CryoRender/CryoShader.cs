﻿using System;
using System.Collections.Generic;
using System.Text;

using VulkanCore;

namespace CryogenicEngine.CryoRender
{
    class CryoShader: IDisposable
    {
        CryoVulkan.CryoVulkan cryoVulkan;
        CryogenicEngine.CryoApp cryoApp;

        CryoRenderer cryoRenderer;

        VulkanCore.ShaderModule vertex;
        VulkanCore.ShaderModule fragment;


        internal DescriptorSetLayout descriptorSetLayout;
        

        internal PipelineLayout pipelineLayout;

        internal Pipeline pipeline;

        public CryoShader()
        {

        }

        internal void Initialize(CryoApp app , CryoRenderer renderer)
        {
            cryoVulkan = CryoVulkan.CryoVulkan.Instance;
            cryoApp = app;
            cryoRenderer = renderer;

            vertex = CryoContent.Loader.LoadShaderModule(cryoVulkan, "Content/Shader/shader.vert.spv");
            fragment = CryoContent.Loader.LoadShaderModule(cryoVulkan, "Content/Shader/shader.frag.spv");

            CreateDescriptorSetLayout();
            CreatePipelineLayout();
            CreateGraphicsPipeline();
        }

        public void Dispose()
        {
            descriptorSetLayout.Dispose();
            pipelineLayout.Dispose();
            pipeline.Dispose();
        }

        void CreateDescriptorSetLayout()
        {
            //TODO add desposing

            //Retreave the engine base desciptors for the layout
            List<DescriptorSetLayoutBinding> descriptor = new List<DescriptorSetLayoutBinding>();
            descriptor.AddRange(CryoRenderer.baseDescriptors);

            //Add the ones that the shader needs
            descriptor.Add(new DescriptorSetLayoutBinding(2, DescriptorType.CombinedImageSampler, 1, ShaderStages.Fragment));

            //Create the layout
            descriptorSetLayout = cryoVulkan.Device.CreateDescriptorSetLayout(new DescriptorSetLayoutCreateInfo(descriptor.ToArray()));
        }

        private void CreatePipelineLayout()
        {
            var layoutCreateInfo = new PipelineLayoutCreateInfo(new[] { descriptorSetLayout });
            pipelineLayout = cryoVulkan.Device.CreatePipelineLayout(layoutCreateInfo);
        }

        private void CreateGraphicsPipeline()
        {
            /*
            var vertexShader = CryoContent.Loader.LoadShaderModule(cryoVulkan, "Content/Shader/shader.vert.spv");
            var fragmentShader = CryoContent.Loader.LoadShaderModule(cryoVulkan, "Content/Shader/shader.frag.spv");
            */

            var shaderStageCreateInfos = new[]
            {
                new PipelineShaderStageCreateInfo(ShaderStages.Vertex, vertex, "main"),
                new PipelineShaderStageCreateInfo(ShaderStages.Fragment, fragment, "main")
            };

            var vertexInputStateCreateInfo = new PipelineVertexInputStateCreateInfo(
                Vertex.getVertexInputBindingDescription(),
                Vertex.getVertexInputAttributeDescription()
            );

            var inputAssemblyStateCreateInfo = new PipelineInputAssemblyStateCreateInfo(PrimitiveTopology.TriangleList);

            var depthStencilCreateInfo = new PipelineDepthStencilStateCreateInfo
            {
                DepthTestEnable = true,
                DepthWriteEnable = true,
                DepthCompareOp = CompareOp.Less,
                StencilTestEnable = false
                /*
                Back = new StencilOpState
                {
                    FailOp = StencilOp.Keep,
                    PassOp = StencilOp.Keep,
                    CompareOp = CompareOp.Always
                },
                Front = new StencilOpState
                {
                    FailOp = StencilOp.Keep,
                    PassOp = StencilOp.Keep,
                    CompareOp = CompareOp.Always
                }
                */
            };

            var viewportStateCreateInfo = new PipelineViewportStateCreateInfo(
                new Viewport(0, 0, cryoApp.Host.window.Width, cryoApp.Host.window.Height),
                new Rect2D(0, 0, cryoApp.Host.window.Width, cryoApp.Host.window.Height)
            );

            var rasterizationStateCreateInfo = new PipelineRasterizationStateCreateInfo
            {
                PolygonMode = PolygonMode.Fill,
                CullMode = CullModes.Back,
                FrontFace = FrontFace.Clockwise,
                LineWidth = 1.0f
            };

            var multisampleStateCreateInfo = new PipelineMultisampleStateCreateInfo
            {
                RasterizationSamples = SampleCounts.Count1,
                MinSampleShading = 1.0f
            };

            var colorBlendAttachmentState = new PipelineColorBlendAttachmentState
            {
                SrcColorBlendFactor = BlendFactor.One,
                DstColorBlendFactor = BlendFactor.Zero,
                ColorBlendOp = BlendOp.Add,
                SrcAlphaBlendFactor = BlendFactor.One,
                DstAlphaBlendFactor = BlendFactor.Zero,
                AlphaBlendOp = BlendOp.Add,
                ColorWriteMask = ColorComponents.All
            };

            var colorBlendStateCreateInfo = new PipelineColorBlendStateCreateInfo(
                new[] { colorBlendAttachmentState });

            var pipelineCreateInfo = new GraphicsPipelineCreateInfo(
                layout: pipelineLayout,
                renderPass: cryoRenderer.renderPass,
                subpass: 0,
                stages: shaderStageCreateInfos,
                inputAssemblyState: inputAssemblyStateCreateInfo,
                vertexInputState: vertexInputStateCreateInfo,
                rasterizationState: rasterizationStateCreateInfo,
                viewportState: viewportStateCreateInfo,
                multisampleState: multisampleStateCreateInfo,
                colorBlendState: colorBlendStateCreateInfo,
                depthStencilState: depthStencilCreateInfo);
            pipeline = cryoVulkan.Device.CreateGraphicsPipeline(pipelineCreateInfo);
        }
    }
}
