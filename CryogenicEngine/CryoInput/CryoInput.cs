﻿using System;
using System.Collections.Generic;
using System.Text;
using SDL2;

namespace CryogenicEngine.CryoInput
{
    public class CryoInput
    {
        CryoApp app;

        public static CryoInput Instance;

        List<InputBinding> bindings = new List<InputBinding>();


        public CryoInput(CryoApp app)
        {
            this.app = app;
            Instance = this;
        }

        public void AddBinding(InputBinding inputBinding)
        {
            bindings.Add(inputBinding);
        }

        public InputBinding GetInputBinding(string name)
        {
            return bindings.Find(i => i.name == name);
        }

        public float GetInputState(string name)
        {
            return bindings.Find(i => i.name == name).state;
        }

        public bool GetBinaryInputState(string name)
        {
            return bindings.Find(i => i.name == name).state != 0 ? true: false;
        }

        internal void RegisterEvent(SDL2.SDL.SDL_Event SDLevent)
        {
            foreach (var item in bindings)
            {
                item.RegisterEvent(SDLevent);
            }
        }

        internal void ClearBindingStates()
        {
            foreach (var item in bindings)
            {
                item.CearState();
            }
        }

    }


    //This will house a single event facing the engine
    //And multiple inputs tha can cause it
    public class InputBinding
    {
        
        /// <summary>
        /// This is the name of the event
        /// </summary>
        public string name;

        /// <summary>
        /// Shows if this ation si currently enable - if not it will not trigger even if the conditions met
        /// </summary>
        bool enabled;

        internal float state;

        internal bool cleared;

        /// <summary>
        /// This contains the different bindings for the input
        /// </summary>
        List<IBindingEvent> events = new List<IBindingEvent>();

        public InputBinding(string name, bool enabled, bool cleared , List<IBindingEvent> events)
        {
            this.name = name;
            this.enabled = enabled;
            this.events = events;
            this.cleared = cleared;
            foreach (var item in events)
            {
                item.Asign(this);
            }

        }

        public void RegisterEvent(SDL2.SDL.SDL_Event SDLevent)
        {
            foreach (var item in events)
            {
                item.NewEvent(SDLevent);
            }
        }

        public void CearState()
        {
            if (cleared)
            {
                state = 0;
            }
        }
    }



    public interface IBindingEvent
    {
        void Asign(InputBinding inputBinding);
        void NewEvent(SDL2.SDL.SDL_Event sdlEvent);
    }

    public class BindingEventKeyboard :IBindingEvent
    {
        SDL2.SDL.SDL_Keycode keycode;

        InputBinding binding;

        public BindingEventKeyboard(SDL.SDL_Keycode keycode)
        {
            this.keycode = keycode;
        }

        public void Asign(InputBinding inputBinding)
        {
            binding = inputBinding;
        }

        public void NewEvent(SDL.SDL_Event sdlEvent)
        {
            if (sdlEvent.type == SDL.SDL_EventType.SDL_KEYDOWN)
            {
                var button = sdlEvent.key.keysym.sym;
                if(button == keycode)
                {
                    binding.state = 1;
                }
            }
            else if (sdlEvent.type == SDL.SDL_EventType.SDL_KEYUP)
            {
                var button = sdlEvent.key.keysym.sym;
                if (button == keycode)
                {
                    binding.state = 0;
                }
            }
            
        }
    }

    public class BindingEventMouse : IBindingEvent
    {
        InputBinding binding;

        Direction direction;

        public enum Direction
        {
            Horizontal,
            Vertical
        }

        public BindingEventMouse(Direction dir)
        {
            direction = dir;
        }

        public void Asign(InputBinding inputBinding)
        {
            binding = inputBinding;
        }

        public void NewEvent(SDL.SDL_Event sdlEvent)
        {
            if (sdlEvent.type == SDL.SDL_EventType.SDL_MOUSEMOTION)
            {
                switch (direction)
                {
                    case Direction.Horizontal:
                        binding.state = sdlEvent.motion.xrel;
                        break;
                    case Direction.Vertical:
                        binding.state = sdlEvent.motion.yrel;
                        break;
                    default:
                        break;
                }
                /*
                int mouseXrel = sdlEvent.motion.xrel;
                int mouseX = sdlEvent.motion.x;
                int mouseYrel = sdlEvent.motion.yrel;
                int mouseY = sdlEvent.motion.y;
                */
                //Console.WriteLine("Rel:  X: {0}", sdlEvent.motion.xrel);
                //Console.WriteLine("Norm: X: {0} Y: {1}", mouseX, mouseY);
            }
        }
    }


}
