﻿using CryogenicEngine.Host;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VulkanCore;
using VulkanCore.Ext;
using VulkanCore.Khr;

namespace CryogenicEngine.CryoVulkan
{
    public class CryoVulkan
    {
        CryoApp cryoApp;

        Instance VKinstance;

        public SurfaceKhr surface;

        public PhysicalDevice PhysicalDevice;
        public Device Device;
        public PhysicalDeviceMemoryProperties MemoryProperties;
        public PhysicalDeviceFeatures Features;
        public PhysicalDeviceProperties Properties;
        public Queue GraphicsQueue;
        public Queue ComputeQueue;
        public Queue PresentQueue;
        public CommandPool GraphicsCommandPool;
        public CommandPool ComputeCommandPool;

        public static CryoVulkan Instance;

        public CryoVulkanBufferManagger bufferManagger;

        public void Initialize(CryoApp app)
        {
            if (Instance != null)
            {
                throw new Exception("More than one instance!");
            }
            Instance = this;

            cryoApp = app;

            CreateInstance();
        }

        void CreateInstance()
        {
            var availableLayers = VulkanCore.Instance.EnumerateLayerProperties();

            var createInfo = new InstanceCreateInfo();
            createInfo.EnabledLayerNames = new[] {
                Constant.InstanceLayer.LunarGStandardValidation}
                .Where(availableLayers.Contains)
                .ToArray();

            createInfo.EnabledExtensionNames = new[]
            {
                    Constant.InstanceExtension.KhrSurface,
                    Constant.InstanceExtension.KhrWin32Surface,
                    Constant.InstanceExtension.ExtDebugReport
            };

            VKinstance = new Instance(createInfo);

            /*
            Todo: figure out if this methode can be used
            IntPtr surface_pointer;
            SDL2.SDL.SDL_Vulkan_CreateSurface(cryoApp.Host.window.GetWindowPointer(),VKinstance,out surface_pointer);
            SurfaceKhr surface = surface_pointer;
            */

            SDLWindow window = cryoApp.Host.window;
            surface = VKinstance.CreateWin32SurfaceKhr(new Win32SurfaceCreateInfoKhr(window.WindowHInstance, window.WindowHandle));

            InstanceInint();

            bufferManagger = new CryoVulkanBufferManagger();
        }

        void InstanceInint()
        {
            // Find graphics and presentation capable physical device(s) that support
            // the provided surface for platform.
            int graphicsQueueFamilyIndex = -1;
            int computeQueueFamilyIndex = -1;
            int presentQueueFamilyIndex = -1;
            foreach (PhysicalDevice physicalDevice in VKinstance.EnumeratePhysicalDevices())
            {
                QueueFamilyProperties[] queueFamilyProperties = physicalDevice.GetQueueFamilyProperties();
                for (int i = 0; i < queueFamilyProperties.Length; i++)
                {
                    if (queueFamilyProperties[i].QueueFlags.HasFlag(Queues.Graphics))
                    {
                        if (graphicsQueueFamilyIndex == -1) graphicsQueueFamilyIndex = i;
                        if (computeQueueFamilyIndex == -1) computeQueueFamilyIndex = i;

                        if (physicalDevice.GetSurfaceSupportKhr(i, surface) &&
                            GetPresentationSupport(physicalDevice, i))
                        {
                            presentQueueFamilyIndex = i;
                        }

                        if (graphicsQueueFamilyIndex != -1 &&
                            computeQueueFamilyIndex != -1 &&
                            presentQueueFamilyIndex != -1)
                        {
                            PhysicalDevice = physicalDevice;
                            break;
                        }
                    }
                }
                if (PhysicalDevice != null) break;
            }

            bool GetPresentationSupport(PhysicalDevice physicalDevice, int queueFamilyIndex)
            {
                //switch (platform)
                //{
                //    case Platform.Android:
                //        return true;
                //    case Platform.Win32:
                //        return physicalDevice.GetWin32PresentationSupportKhr(queueFamilyIndex);
                //    default:
                //        throw new NotImplementedException();
                //}
                
                return physicalDevice.GetWin32PresentationSupportKhr(queueFamilyIndex);
            }

            if (PhysicalDevice == null)
                throw new InvalidOperationException("No suitable physical device found.");

            // Store memory properties of the physical device.
            MemoryProperties = PhysicalDevice.GetMemoryProperties();
            Features = PhysicalDevice.GetFeatures();
            Properties = PhysicalDevice.GetProperties();

            // Create a logical device.
            bool sameGraphicsAndPresent = graphicsQueueFamilyIndex == presentQueueFamilyIndex;
            var queueCreateInfos = new DeviceQueueCreateInfo[sameGraphicsAndPresent ? 1 : 2];
            queueCreateInfos[0] = new DeviceQueueCreateInfo(graphicsQueueFamilyIndex, 1, 1.0f);
            if (!sameGraphicsAndPresent)
                queueCreateInfos[1] = new DeviceQueueCreateInfo(presentQueueFamilyIndex, 1, 1.0f);

            var deviceCreateInfo = new DeviceCreateInfo(
                queueCreateInfos,
                new[] { Constant.DeviceExtension.KhrSwapchain },
                Features);
            Device = PhysicalDevice.CreateDevice(deviceCreateInfo);

            // Get queue(s).
            GraphicsQueue = Device.GetQueue(graphicsQueueFamilyIndex);
            ComputeQueue = computeQueueFamilyIndex == graphicsQueueFamilyIndex
                ? GraphicsQueue
                : Device.GetQueue(computeQueueFamilyIndex);
            PresentQueue = presentQueueFamilyIndex == graphicsQueueFamilyIndex
                ? GraphicsQueue
                : Device.GetQueue(presentQueueFamilyIndex);

            // Create command pool(s).
            GraphicsCommandPool = Device.CreateCommandPool(new CommandPoolCreateInfo(graphicsQueueFamilyIndex, CommandPoolCreateFlags.ResetCommandBuffer));
            ComputeCommandPool = Device.CreateCommandPool(new CommandPoolCreateInfo(computeQueueFamilyIndex));
        }

        public void Dispose()
        {
            ComputeCommandPool.Dispose();
            GraphicsCommandPool.Dispose();
            Device.Dispose();
        }

        public CryoVulkanBuffer createBuffer(long size, BufferUsages bufferUsages, MemoryProperties properties)
        {
            //long size = vertices.Length * Interop.SizeOf<Vertex>();

            BufferCreateInfo bufferCreateInfo = new BufferCreateInfo();
            bufferCreateInfo.Size = size;
            bufferCreateInfo.Usage = bufferUsages;
            bufferCreateInfo.SharingMode = SharingMode.Exclusive;

            //TODO Error check
            var buffer = Device.CreateBuffer(bufferCreateInfo);

            MemoryRequirements stagingReq = buffer.GetMemoryRequirements();
            int vertexMemoryTypeIndex = MemoryProperties.MemoryTypes.IndexOf(
                stagingReq.MemoryTypeBits,
                properties);

            //TODO Error check
            DeviceMemory memory = Device.AllocateMemory(new MemoryAllocateInfo(stagingReq.Size, vertexMemoryTypeIndex));

            buffer.BindMemory(memory);

            return new CryoVulkanBuffer(buffer, memory, size);
        }

        public CommandBuffer beginSingleTimeCommands()
        {
            CommandBuffer cmdBuffer = GraphicsCommandPool.AllocateBuffers(new CommandBufferAllocateInfo(CommandBufferLevel.Primary, 1))[0];
            cmdBuffer.Begin(new CommandBufferBeginInfo(CommandBufferUsages.OneTimeSubmit));

            return cmdBuffer;
        }

        public void endSingleTimeCommands(CommandBuffer cmdBuffer)
        {
            cmdBuffer.End();

            Fence fence = Device.CreateFence();
            GraphicsQueue.Submit(new SubmitInfo(commandBuffers: new[] { cmdBuffer }), fence);
            fence.Wait();

            fence.Dispose();
        }

    }
}
