﻿using System;
using System.Collections.Generic;
using System.Text;
using VulkanCore;

namespace CryogenicEngine.CryoVulkan
{
    public class CryoVulkanBuffer : IDisposable
    {

        public CryoVulkanBuffer(VulkanCore.Buffer buffer, DeviceMemory memory, long size)
        {
            Buffer = buffer;
            Memory = memory;
            Size = size;
        }

        public VulkanCore.Buffer Buffer { get; }
        public DeviceMemory Memory { get; }
        public long Size { get; }

        public void Dispose()
        {
            Memory.Dispose();
            Buffer.Dispose();
        }

        public static implicit operator VulkanCore.Buffer(CryoVulkanBuffer value) => value.Buffer;
    }
}
