﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using VulkanCore;
using CryogenicEngine.CryoRender;

namespace CryogenicEngine.CryoVulkan
{
    public class CryoVulkanBufferManagger
    {
        List<BufferPage> pages = new List<BufferPage>();

        public static CryoVulkanBufferManagger Instance;

        internal long minOffset;

        public CryoVulkanBufferManagger()
        {
            Instance = this;

            minOffset = CryoVulkan.Instance.PhysicalDevice.GetProperties().Limits.MinUniformBufferOffsetAlignment;

            pages.Add(new BufferPage(5000, BufferUsages.IndexBuffer,256));
            pages.Add(new BufferPage(5000, BufferUsages.VertexBuffer,256));
            pages.Add(new BufferPage(5000, BufferUsages.UniformBuffer, minOffset, false));

            
        }


        public CryoMannagedBuffer CreateBuffer(BufferUsages bufferTypes, long size, object data)
        {
            var type = data.GetType();

            var BufferPage = pages.Where(i => i.bufferType == bufferTypes).Where(i => i.FreeSpace >= size).FirstOrDefault();
            if (BufferPage == null)
            {
                //TODO create buffer page and check if we still have left
                throw new Exception("No correct page buffer");
            }

            return BufferPage.CreateNewBuffer(size, data);

        }

        public CryoMannagedBuffer CreateBuffer(BufferUsages bufferTypes, long size)
        {
            var BufferPage = pages.Where(i => i.bufferType == bufferTypes).Where(i => i.FreeSpace >= size).FirstOrDefault();
            if (BufferPage == null)
            {
                //TODO create buffer page and check if we still have left
                throw new Exception("No correct page buffer");
            }

            return BufferPage.CreateNewBuffer(size);
        }

    }

    public class BufferPage
    {
        public BufferUsages bufferType;

        SortedList<long, CryoMannagedBuffer> list = new SortedList<long, CryoMannagedBuffer>();

        public BufferData StagingBuffer { get; }

        public BufferData DeviceBuffer { get; }

        public long Size { get; }

        public long DataEnd;
        public long FreeSpace;

        public bool DeviceReplicated;

        long minOffset;

        public BufferPage(long size, BufferUsages usages,long minOffset , bool deviceReplicated = true)
        {
            StagingBuffer = CreateBuffer(size, BufferUsages.TransferSrc | (deviceReplicated ? 0 : usages), MemoryProperties.HostVisible | MemoryProperties.HostCoherent);

            if (deviceReplicated)
            {
                DeviceBuffer = CreateBuffer(size, BufferUsages.TransferDst | usages, MemoryProperties.DeviceLocal);
            }

            FreeSpace = size;
            Size = size;
            bufferType = usages;

            DeviceReplicated = deviceReplicated;

            this.minOffset = minOffset;
        }

        public CryoMannagedBuffer CreateNewBuffer(long size, object data)
        {
            CryoMannagedBuffer buffer = CreateNewBuffer(size);
            buffer.UpdateData(data);
            return buffer;
        }

        public CryoMannagedBuffer CreateNewBuffer(long size)
        {
            long offset = DataEnd;

            long offsetSize = (long)(MathF.Ceiling(size / (minOffset*1.0f)) * minOffset);

            var res = new CryoMannagedBuffer(offset, offsetSize, this);

            DataEnd += offsetSize;
            FreeSpace -= offsetSize;

            return res;
        }

        //TODO syncronise the copy
        public void UpdateData(CryoMannagedBuffer mannagedBuffer, object data)
        {
            long offset = mannagedBuffer.Offset;


            IntPtr vertexPtr = StagingBuffer.Memory.Map(offset, mannagedBuffer.Size);

            if (data is int[])
                Interop.Write(vertexPtr, (int[])data);
            else if (data is Vertex[])
                Interop.Write(vertexPtr, (Vertex[])data);
            else if (data is CameraBufferObject)
            {
                CameraBufferObject uob = (CameraBufferObject)data;
                Interop.Write(vertexPtr, ref uob);
            }
            else if (data is CryoMath.Matrix4x4)
            {
                CryoMath.Matrix4x4 uob = (CryoMath.Matrix4x4)data;
                Interop.Write(vertexPtr, ref uob);
            }
            else
            {
                throw new Exception("Error: could not detect buffer object type");
            }
            StagingBuffer.Memory.Unmap();

            if (DeviceReplicated)
            {
                CommandBuffer cmd = CryoVulkan.Instance.beginSingleTimeCommands();

                cmd.CmdCopyBuffer(StagingBuffer, DeviceBuffer, new BufferCopy(mannagedBuffer.Size, offset, offset));

                CryoVulkan.Instance.endSingleTimeCommands(cmd);
            }

        }

        private BufferData CreateBuffer(long size, BufferUsages usages, MemoryProperties properties)
        {
            CryoVulkan cryoVulkan = CryoVulkan.Instance;

            //long size = vertices.Length * Interop.SizeOf<Vertex>();

            BufferCreateInfo bufferCreateInfo = new BufferCreateInfo
            {
                Size = size,
                Usage = usages,
                SharingMode = SharingMode.Exclusive
            };


            //TODO Error check
            var buffer = CryoVulkan.Instance.Device.CreateBuffer(bufferCreateInfo);

            MemoryRequirements stagingReq = buffer.GetMemoryRequirements();

            int vertexMemoryTypeIndex = cryoVulkan.MemoryProperties.MemoryTypes.IndexOf(
                stagingReq.MemoryTypeBits,
                properties);

            //TODO Error check
            DeviceMemory memory = cryoVulkan.Device.AllocateMemory(new MemoryAllocateInfo(stagingReq.Size, vertexMemoryTypeIndex));

            buffer.BindMemory(memory);

            return new BufferData
            {
                Buffer = buffer,
                Memory = memory,
                Size = size
            };
        }

        public struct BufferData
        {
            public VulkanCore.Buffer Buffer;
            public DeviceMemory Memory;
            public long Size;

            public static implicit operator VulkanCore.Buffer(BufferData value) => value.Buffer;
        }
    }

    public class CryoMannagedBuffer
    {
        public long Offset;
        public long Size;
        public BufferPage Page { get; }

        public static implicit operator VulkanCore.Buffer(CryoMannagedBuffer value) {

            if (value.Page.DeviceReplicated)
            {
                return value.Page.DeviceBuffer;
            }
            else
            {
                return value.Page.StagingBuffer;
            }

        }

        public CryoMannagedBuffer(long offset, long size, BufferPage parent)
        {
            Offset = offset;
            Size = size;
            Page = parent;
        }

        internal void UpdateData(object data)
        {
            Page.UpdateData(this, data);
        }
    }

}
