﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VulkanCore;

namespace CryogenicEngine.CryoVulkan
{
    internal class TextureData
    {
        public Mipmap[] Mipmaps { get; set; }
        public Format Format { get; set; }

        public class Mipmap
        {
            public byte[] Data { get; set; }
            public Extent3D Extent { get; set; }
            public int Size { get; set; }
        }
    }

    public class CryoVulkanImage : IDisposable
    {
        private CryoVulkanImage(Image image, DeviceMemory memory, ImageView view, Format format)
        {
            Image = image;
            Memory = memory;
            View = view;
            Format = format;
        }

        public Format Format { get; }
        public Image Image { get; }
        public ImageView View { get; }
        public DeviceMemory Memory { get; }

        public void Dispose()
        {
            View.Dispose();
            Memory.Dispose();
            Image.Dispose();
        }

        public static implicit operator Image(CryoVulkanImage value) => value.Image;


        internal static CryoVulkanImage Texture2D(TextureData tex2D)
        {
            CryoVulkan ctx = CryoVulkan.Instance;

            //Staging buffer on the CPU side
            var stagingBuffer = ctx.createBuffer(tex2D.Mipmaps[0].Size, BufferUsages.TransferSrc, MemoryProperties.HostVisible | MemoryProperties.HostCoherent);

            IntPtr dataptr = stagingBuffer.Memory.Map(0, stagingBuffer.Size);
            Interop.Write(dataptr, tex2D.Mipmaps[0].Data);
            stagingBuffer.Memory.Unmap();

            //Create ImageHolder
            ImageCreateInfo imageInfo = new ImageCreateInfo
            {
                ImageType = ImageType.Image2D,
                Extent = tex2D.Mipmaps[0].Extent,
                MipLevels = 1,
                ArrayLayers = 1,
                Format = tex2D.Format,
                Tiling = ImageTiling.Linear,
                InitialLayout = ImageLayout.Undefined,
                Usage = ImageUsages.Sampled | ImageUsages.TransferDst,
                SharingMode = SharingMode.Exclusive,
                Samples = SampleCounts.Count1
            };
            Image image = ctx.Device.CreateImage(imageInfo);

            //GPU side mem for the image
            MemoryRequirements imageMemReq = image.GetMemoryRequirements();

            int imageHeapIndex = ctx.MemoryProperties.MemoryTypes.IndexOf(
                imageMemReq.MemoryTypeBits, MemoryProperties.DeviceLocal);

            DeviceMemory memory = ctx.Device.AllocateMemory(new MemoryAllocateInfo(imageMemReq.Size, imageHeapIndex));
            image.BindMemory(memory);

            //Command buffer to copy the data from Staging to final
            CommandBuffer commandBuffer = ctx.beginSingleTimeCommands();

            BufferImageCopy copyRegion = new BufferImageCopy
            {
                ImageSubresource = new ImageSubresourceLayers(ImageAspects.Color, 0, 0, 1),
                ImageExtent = tex2D.Mipmaps[0].Extent,
                BufferOffset = 0
            };

            transitionImageLayout(commandBuffer,image, tex2D.Format, ImageLayout.Undefined, ImageLayout.TransferDstOptimal);

            commandBuffer.CmdCopyBufferToImage(stagingBuffer, image, ImageLayout.TransferDstOptimal, copyRegion );

            transitionImageLayout(commandBuffer,image, tex2D.Format, ImageLayout.TransferDstOptimal, ImageLayout.ShaderReadOnlyOptimal);

            ctx.endSingleTimeCommands(commandBuffer);

            // Create image view.
            var subresourceRange = new ImageSubresourceRange(ImageAspects.Color, 0, tex2D.Mipmaps.Length, 0, 1);

            ImageView view = image.CreateView(new ImageViewCreateInfo(tex2D.Format, subresourceRange));

            return new CryoVulkanImage(image, memory, view, tex2D.Format);
        }

        internal static CryoVulkanImage DepthStencil(int width, int height)
        {
            Format[] validFormats =
            {
                Format.D32SFloatS8UInt,
                Format.D32SFloat,
                Format.D24UNormS8UInt,
                Format.D16UNormS8UInt,
                Format.D16UNorm
            };

            Format? potentialFormat = validFormats.FirstOrDefault(
                validFormat =>
                {
                    FormatProperties formatProps = CryoVulkan.Instance.PhysicalDevice.GetFormatProperties(validFormat);
                    return (formatProps.OptimalTilingFeatures & FormatFeatures.DepthStencilAttachment) > 0;
                });

            if (!potentialFormat.HasValue)
                throw new InvalidOperationException("Required depth stencil format not supported.");

            Format format = potentialFormat.Value;

            Image image = CryoVulkan.Instance.Device.CreateImage(new ImageCreateInfo
            {
                ImageType = ImageType.Image2D,
                Format = format,
                Extent = new Extent3D(width, height, 1),
                MipLevels = 1,
                ArrayLayers = 1,
                Samples = SampleCounts.Count1,
                Tiling = ImageTiling.Optimal,
                Usage = ImageUsages.DepthStencilAttachment | ImageUsages.TransferSrc
            });
            MemoryRequirements memReq = image.GetMemoryRequirements();
            int heapIndex = CryoVulkan.Instance.MemoryProperties.MemoryTypes.IndexOf(
                memReq.MemoryTypeBits, MemoryProperties.DeviceLocal);
            DeviceMemory memory = CryoVulkan.Instance.Device.AllocateMemory(new MemoryAllocateInfo(memReq.Size, heapIndex));
            image.BindMemory(memory);
            ImageView view = image.CreateView(new ImageViewCreateInfo(format,
                new ImageSubresourceRange(ImageAspects.Depth | ImageAspects.Stencil, 0, 1, 0, 1)));

            return new CryoVulkanImage(image, memory, view, format);
        }

        static void transitionImageLayout(Image image, Format format, ImageLayout oldLayout, ImageLayout newLayout)
        {
            CommandBuffer commandBuffer = CryoVulkan.Instance.beginSingleTimeCommands();

            transitionImageLayout(image, format, oldLayout, newLayout);

            CryoVulkan.Instance.endSingleTimeCommands(commandBuffer);
        }

        static void transitionImageLayout(CommandBuffer commandBuffer, Image image, Format format, ImageLayout oldLayout, ImageLayout newLayout)
        {
            //CommandBuffer commandBuffer = CryoVulkan.Instance.beginSingleTimeCommands();

            ImageMemoryBarrier barrier = new ImageMemoryBarrier();
            barrier.OldLayout = oldLayout;
            barrier.NewLayout = newLayout;
            barrier.SrcQueueFamilyIndex = -1;
            barrier.DstQueueFamilyIndex = -1;
            barrier.Image = image;

            if (newLayout == ImageLayout.DepthStencilAttachmentOptimal)
            {
                barrier.SubresourceRange.AspectMask = ImageAspects.Depth;

                if (hasStencilComponent(format))
                {
                    barrier.SubresourceRange.AspectMask |= ImageAspects.Stencil;
                }
            }
            else
            {
                barrier.SubresourceRange.AspectMask = ImageAspects.Color;
            }

            barrier.SubresourceRange.BaseMipLevel = 0;
            barrier.SubresourceRange.LevelCount = 1;
            barrier.SubresourceRange.BaseArrayLayer = 0;
            barrier.SubresourceRange.LayerCount = 1;

            PipelineStages sourceStage;
            PipelineStages destinationStage;

            if (oldLayout == ImageLayout.Undefined && newLayout == ImageLayout.TransferDstOptimal)
            {
                barrier.SrcAccessMask = 0;
                barrier.DstAccessMask = Accesses.TransferWrite;

                sourceStage = PipelineStages.TopOfPipe;
                destinationStage = PipelineStages.Transfer;
            }
            else if (oldLayout == ImageLayout.TransferDstOptimal && newLayout == ImageLayout.ShaderReadOnlyOptimal)
            {
                barrier.SrcAccessMask = Accesses.TransferWrite;
                barrier.DstAccessMask = Accesses.ShaderRead;

                sourceStage = PipelineStages.Transfer;
                destinationStage = PipelineStages.FragmentShader;
            }
            else if (oldLayout == ImageLayout.Undefined && newLayout == ImageLayout.DepthStencilAttachmentOptimal)
            {
                barrier.SrcAccessMask = 0;
                barrier.DstAccessMask = Accesses.DepthStencilAttachmentRead | Accesses.DepthStencilAttachmentWrite;

                sourceStage = PipelineStages.TopOfPipe;
                destinationStage = PipelineStages.EarlyFragmentTests;
            }
            else
            {
                throw new Exception("unsupported layout transition!");
            }

            commandBuffer.CmdPipelineBarrier(
                sourceStage, destinationStage,
                0,
                null,
                null,
                new[] { barrier }
            );

            //CryoVulkan.Instance.endSingleTimeCommands(commandBuffer);
        }

        static bool hasStencilComponent(Format format)
        {
            return format == Format.D32SFloatS8UInt || format == Format.D24UNormS8UInt;
        }
    }
}
